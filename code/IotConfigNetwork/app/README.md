# base

#### 集成流程：
## 1.集成base
    a.在根项目的settings.gradle.kts文件中添加引用地址：
    include(":base")
    project(":base").projectDir=File("xxxx/AndroidKotlinBaseProject/base")
    
    b.导包后，其它所有默认导包都可以删除掉（不包含测试模块的，所以如果有测试块，还是要添加相应的测试包的）， 如下：
    dependencies {
        implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
        implementation(project(mapOf("path" to ":base")))
    
    }

    c.项目项目引用base的，如下：
    <style name="Theme.xxxx" parent="@style/Theme.Digua.Base" />
    
    d.必需自定Application
    

#### 集成时各种报错处理
## 1.SmartRefreshLayout报错
    如果使用 AndroidX 在 gradle.properties 中添加
    android.useAndroidX=true
    android.enableJetifier=true

