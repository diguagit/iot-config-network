plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

android {
    namespace = "com.digua.iot.confignetwork"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.digua.iot.confignetwork"
        minSdk = 26
        targetSdk = 33
        versionCode = 100
        versionName = "1.0.0"

        vectorDrawables {
            useSupportLibrary = true
        }
    }

    signingConfigs {
        getByName("debug") {
            storeFile = file("jks.jks")
            storePassword = "123456"
            keyAlias = "key0"
            keyPassword = "123456"
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            resValue("string", "app_name", "IOT配网")
            buildConfigField("String", "appName", "\"IotConfigNetwork\"")
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
        debug {
            isMinifyEnabled = false
            resValue("string", "app_name", "IOT配网-测试")
            buildConfigField("String", "appName", "\"IotConfigNetwork\"")
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions { jvmTarget = "17" }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions { kotlinCompilerExtensionVersion = "1.4.3" }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    sourceSets.all { jniLibs.srcDirs("libs") }
    viewBinding { enable = true }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(project(mapOf("path" to ":base")))

    //物联网连接模块
    //implementation("com.rairmmd:andesptouch:1.0.5")
    //implementation("com.github.EspressifApp:lib-esptouch-android:1.1.1")
    //implementation("com.github.EspressifApp:lib-esptouch-v2-android:2.2.1")
}