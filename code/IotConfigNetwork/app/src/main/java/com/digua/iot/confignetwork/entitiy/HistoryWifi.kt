package com.digua.iot.confignetwork.entitiy

import java.io.Serializable

class HistoryWifi : Serializable {
    var name: String? = null
    var pwd: String? = null
    var ssid: String? = null
    var bssid: String? = null
}